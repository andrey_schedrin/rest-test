<?php

use Illuminate\Support\Facades\Route;

Route::prefix('posts')->group(function () {
    require_once "Modules/Posts/routes.php";
});
