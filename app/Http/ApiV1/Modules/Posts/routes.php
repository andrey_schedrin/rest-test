<?php

use App\Http\ApiV1\Modules\Posts\Controllers\PostRatingsController;
use App\Http\ApiV1\Modules\Posts\Controllers\PostsController;
use Illuminate\Support\Facades\Route;

Route::get('posts/{postId}', [PostsController::class, 'get']);
Route::post('posts', [PostsController::class, 'create']);
Route::patch('posts/{postId}', [PostsController::class, 'patch']);
Route::put('posts/{postId}', [PostsController::class, 'replace']);
Route::delete('posts/{postId}', [PostsController::class, 'delete']);
Route::post('posts:search', [PostsController::class, 'search']);

Route::post('ratings', [PostRatingsController::class, 'create']);
Route::delete('ratings/{ratingId}', [PostRatingsController::class, 'delete']);
Route::post('ratings/{ratingId}:revert', [PostRatingsController::class, 'revert']);
