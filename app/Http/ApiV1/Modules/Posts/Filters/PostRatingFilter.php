<?php

namespace App\Http\ApiV1\Modules\Posts\Filters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class PostRatingFilter implements Filter
{
    public function __construct(protected string $operand = '=') {}

    public function __invoke(Builder $query, $value, string $property)
    {
        $query->fromRaw(
            '(SELECT posts.*, SUM(CASE WHEN "like" = true THEN 1 when "like" = false THEN -1 ELSE 0 END)
            FROM posts LEFT JOIN ratings on posts.id = ratings.post_id GROUP BY posts.id) as rating'
        );
        $query->where('sum', $this->operand, $value);
    }
}
