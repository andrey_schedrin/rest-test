<?php

namespace App\Http\ApiV1\Modules\Posts\Resources;

use App\Domain\Posts\Models\Rating;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin Rating */
class RatingsResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'post_id' => $this->post_id,
            'like' => $this->like,
        ];
    }
}
