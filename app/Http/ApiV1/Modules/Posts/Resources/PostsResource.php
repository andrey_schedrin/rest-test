<?php

namespace App\Http\ApiV1\Modules\Posts\Resources;

use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Illuminate\Http\Request;

/** @mixin Post */
class PostsResource extends BaseJsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'title' => $this->title,
            'preview' => $this->preview,
            'content' => $this->content,
        ];

        if ($this->relationLoaded('ratings')) {
            $data['rating'] = $this->rating;
        }

        return $data;
    }
}
