<?php

namespace App\Http\ApiV1\Modules\Posts\Controllers;

use App\Domain\Posts\Actions\CreatePostAction;
use App\Domain\Posts\Actions\DeletePostAction;
use App\Domain\Posts\Actions\UpdatePostAction;
use App\Http\ApiV1\Modules\Posts\Queries\PostsQuery;
use App\Http\ApiV1\Modules\Posts\Requests\CreateOrReplacePostRequest;
use App\Http\ApiV1\Modules\Posts\Requests\PatchPostRequest;
use App\Http\ApiV1\Modules\Posts\Resources\PostsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class PostsController extends Controller
{
    public function get(int $postId, PostsQuery $query)
    {
        return new PostsResource($query->findOrFail($postId));
    }

    public function create(CreateOrReplacePostRequest $request, CreatePostAction $action): PostsResource
    {
        return new PostsResource($action->execute($request->validated()));
    }

    public function patch(PatchPostRequest $request, UpdatePostAction $action, int $postId): PostsResource
    {
        return new PostsResource($action->execute($request->validated(), $postId));
    }

    public function replace(CreateOrReplacePostRequest $request, UpdatePostAction $action, int $postId): PostsResource
    {
        return new PostsResource($action->execute($request->validated(), $postId));
    }

    public function delete(int $postId, DeletePostAction $action): EmptyResource
    {
        $action->execute($postId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PostsQuery $query): ResourceCollection
    {
        return PostsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
