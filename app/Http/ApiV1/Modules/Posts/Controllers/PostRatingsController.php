<?php

namespace App\Http\ApiV1\Modules\Posts\Controllers;

use App\Domain\Posts\Actions\CreateRatingAction;
use App\Domain\Posts\Actions\DeleteRatingAction;
use App\Domain\Posts\Actions\ReversRatingAction;
use App\Http\ApiV1\Modules\Posts\Queries\PostRatingsQuery;
use App\Http\ApiV1\Modules\Posts\Requests\CreateRatingRequest;
use App\Http\ApiV1\Modules\Posts\Resources\RatingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;

class PostRatingsController extends Controller
{
    public function create(CreateRatingRequest $request, CreateRatingAction $action): RatingsResource
    {
        return new RatingsResource($action->execute($request->validated()));
    }

    public function delete(int $ratingId, DeleteRatingAction $action): EmptyResource
    {
        $action->execute($ratingId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, PostRatingsQuery $query): ResourceCollection
    {
        return RatingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function revert(int $ratingId, ReversRatingAction $action): RatingsResource
    {
        return new RatingsResource($action->execute($ratingId));
    }
}
