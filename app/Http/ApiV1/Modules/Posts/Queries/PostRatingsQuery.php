<?php

namespace App\Http\ApiV1\Modules\Posts\Queries;

use App\Domain\Posts\Models\Rating;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class PostRatingsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Rating::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedFilters([
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('post_id'),
        ]);
    }
}
