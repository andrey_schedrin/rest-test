<?php

namespace App\Http\ApiV1\Modules\Posts\Queries;

use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Modules\Posts\Filters\PostRatingFilter;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class PostsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Post::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedSorts(['id', 'created_at', 'updated_at']);

        $this->allowedIncludes(['ratings']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('title'),
            AllowedFilter::custom('rating', new PostRatingFilter()),
            AllowedFilter::custom('rating_lower', new PostRatingFilter('>')),
            AllowedFilter::custom('rating_greater', new PostRatingFilter('<')),
            AllowedFilter::partial('preview'),
        ]);

        $this->defaultSort('id');
    }
}
