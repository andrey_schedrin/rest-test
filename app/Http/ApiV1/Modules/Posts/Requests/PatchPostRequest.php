<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchPostRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['integer', 'min:1'],
            'title' => ['string'],
            'preview' => ['string'],
            'content' => ['string']
        ];
    }
}
