<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplacePostRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'min:1'],
            'title' => ['required', 'string'],
            'preview' => ['required', 'string'],
            'content' => ['string']
        ];
    }
}
