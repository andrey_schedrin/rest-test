<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Domain\Posts\Models\Rating;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateRatingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
                'integer',
                'min:1',
                function ($attribute, $value, $fail) {
                    $exist = Rating::where(['post_id' => $this->post_id, 'user_id' => $this->user_id])->first();
                    if ($exist) {
                        $fail("user_id + post_id don't unique");
                    }
                }
            ],
            'post_id' => ['required', 'integer', 'exists:posts,id'],
            'like' => ['required', 'boolean'],
        ];
    }
}
