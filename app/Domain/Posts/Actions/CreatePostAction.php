<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class CreatePostAction
{
    public function execute(array $data)
    {
        return Post::create($data);
    }
}
