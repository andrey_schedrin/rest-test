<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UpdatePostAction
{
    /** @throws ModelNotFoundException */
    public function execute(array $data, int $postId): Post
    {
        $post = Post::findOrFail($postId);
        $post->update($data);

        return $post;
    }
}
