<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Rating;

class ResetRatingAction
{
    public function execute(int $postId): void
    {
        $ratings = Rating::where('post_id', $postId)->get();
        Rating::destroy($ratings);
    }
}
