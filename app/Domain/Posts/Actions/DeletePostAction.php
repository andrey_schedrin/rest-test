<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class DeletePostAction
{
    public function execute(int $postId): void
    {
        Post::destroy($postId);
    }
}
