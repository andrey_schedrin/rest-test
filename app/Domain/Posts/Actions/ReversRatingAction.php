<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Rating;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReversRatingAction
{
    /** @throws ModelNotFoundException */
    public function execute(int $id): Rating
    {
        $rating = Rating::findOrFail($id);
        $rating->update(['like' => !$rating->like]);

        return $rating;
    }
}
