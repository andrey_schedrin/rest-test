<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Rating;

class CreateRatingAction
{
    public function execute(array $data): Rating
    {
        return Rating::create($data);
    }
}
