<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Rating;

class DeleteRatingAction
{
    public function execute(int $postId): void
    {
        Rating::destroy($postId);
    }
}
