<?php

namespace App\Domain\Posts\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Domain\Posts\Models\Post
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $preview
 * @property string|null $content
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static Builder|Post query()
 * @method static Builder|Post whereContent($value)
 * @method static Builder|Post whereCreatedAt($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post wherePreview($value)
 * @method static Builder|Post whereTitle($value)
 * @method static Builder|Post whereUpdatedAt($value)
 * @method static Builder|Post whereUserId($value)
 * @mixin Eloquent
 * @property-read int $rating
 * @property-read Collection|Rating[] $ratings
 * @property-read int|null $ratings_count
 */
class Post extends Model
{
    protected $guarded = [];
    protected $appends = ['rating'];

    public function ratings(): HasMany
    {
        return $this->hasMany(Rating::class);
    }

    public function getRatingAttribute(): int
    {
        return $this->ratings->sum(function (Rating $rating) {
            return $rating->like ? 1 : -1;
        });
    }
}
