<?php

namespace App\Domain\Posts\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Domain\Posts\Models\Rating
 *
 * @property int $user_id
 * @property int $post_id
 * @property int $like
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Post|null $ratings
 * @method static Builder|Rating newModelQuery()
 * @method static Builder|Rating newQuery()
 * @method static Builder|Rating query()
 * @method static Builder|Rating whereCreatedAt($value)
 * @method static Builder|Rating whereLike($value)
 * @method static Builder|Rating wherePostId($value)
 * @method static Builder|Rating whereUpdatedAt($value)
 * @method static Builder|Rating whereUserId($value)
 * @mixin Eloquent
 */
class Rating extends Model
{
    protected $guarded = [];
    protected $table = 'ratings';
    protected $casts = ['like' => 'bool'];

    public function post(): HasOne
    {
        return $this->hasOne(Post::class);
    }
}
