<?php

namespace App\Console\Commands;

use App\Domain\Posts\Actions\ResetRatingAction;
use Illuminate\Console\Command;

class ResetRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:reset-rating {post_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset rating for the post';


    public function handle(ResetRatingAction $action)
    {
        $postId = (int)$this->argument('post_id');

        $action->execute($postId);

        $this->info("rating for post {$postId} reset");
    }
}
